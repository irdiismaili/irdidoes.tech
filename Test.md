# Nextcloud Deployment with Keycloak SSO – Retrospective Documentation

This document details the steps and decisions taken during the deployment of Nextcloud (integrated with Keycloak SSO) on a test virtual machine. It covers VM preparation, Docker setup, the Nextcloud deployment structure, Docker Compose configuration with Nginx/Certbot for reverse proxy and SSL, the Nextcloud UI setup (including backups and recovery testing), and finally the workaround implemented for Nextcloud AIO’s user limit.

## Table of Contents
1. [VM Readiness Check](#vm-readiness-check)
2. [Install Docker and Docker Compose](#install-docker-and-docker-compose)
3. [Prepare Nextcloud Deployment Structure](prepare-nextcloud-deployment-structure)
4. [Write & Deploy the Docker-Compose Configuration](#write--deploy-the-docker-compose-configuration)
5. [Set Up Nextcloud, Backups & Test Recovery](#set-up-nextcloud-backups--test-recovery)
6. [Adjust Nextcloud AIO User Limit](#adjust-nextcloud-aio-user-limit)

---

## 1. VM Readiness Check

### Overview
The initial phase ensured that the virtual machine was configured securely and had adequate resources for the upcoming deployment tasks.

### What Was Done
- **Hardware Verification:**  
  - Provisioned with **4 CPUs** and **16GB RAM**.
  
- **SSH Configuration:**  
  - SSH keys were installed for both `root` and `extadmin`.
  - **Key Locations:**  
    - `extadmin`: `/home/extadmin/.ssh/authorized_keys`
    - `root`: `/root/.ssh/authorized_keys`
  - Both keys include the comment “Irdi’s ssh key.”

- **Security Considerations:**  
  - In the test environment, both users had SSH key access.  
  - **Production Recommendations:**  
    - Disable password authentication.
    - Prohibit direct root login; restrict access to SSH keys only.

- **Network Configuration:**  
  - Opened ports **80** and **443** to facilitate SSL certificate issuance via Let’s Encrypt.

---

## 2. Install Docker and Docker Compose

### Overview
This phase involved installing containerization tools to support the Nextcloud deployment.

### What Was Done
- **Installation Process:**  
  - Docker and Docker Compose were installed using an archived tutorial from ComputingForgeeks.  
    - **Reference:** [Install Docker and Docker Compose on Debian (Archived)](https://web.archive.org/web/20240921195429/https://computingforgeeks.com/install-docker-and-docker-compose-on-debian/)
  - Although Nextcloud AIO offers a Docker installation script, it was not used because it is not recommended for production.

- **Storage Issue & Resolution:**  
  - **Problem:** The default Docker root partition provided only **3GB** of space.
  - **Solution:**  
    - Decided to either enlarge the root partition (to ~30GB) or change Docker’s root directory.
    - **Reference:** [Change Docker Data Directory (Archived)](https://webcf.waybackmachine.org/web/20250221103325/https://linuxiac.com/how-to-change-docker-data-directory)

---

## 3. Prepare Nextcloud Deployment Structure {prepare-nextcloud-deployment-structure}

### Overview
This task involved planning the data and backup directories and setting the groundwork for reverse proxy and SSL management.

### What Was Done
- **Directory Planning:**  
  - For the test environment, the base directory was set to `/home/$user/nextcloud` (for example, `/home/extadmin/nextcloud`).
  - **Data Directory:**  
    - Inside the base directory, a `data` folder was created to store Nextcloud files (i.e., `/home/extadmin/nextcloud/data`).
  - **Production Consideration:**  
    - In production, the `/home/$user/nextcloud` directory will be replaced with the designated primary data location, and the backup storage will be set to a resizable location.

- **Reverse Proxy & SSL Considerations:**  
  - The deployment used Nginx (installed on the host) for reverse proxy and Certbot for SSL certificate management.
  - **Certificate Troubleshooting:**  
    - Initial issues with obtaining certificates were due to closed ports.
    - Once ports **80/443** were confirmed open (with Kris’ assistance), the certificate was successfully obtained.

---

## 4. Write & Deploy the Docker-Compose Configuration

### Overview
This phase details the deployment of the Nextcloud AIO master container using Docker Compose, along with the external configuration of Nginx and Certbot for reverse proxy and SSL termination.

### What Was Done

#### Docker Compose Setup
- **Container Deployment:**  
  - A Docker Compose file was created to pull the latest Nextcloud AIO master container, configure persistent storage, and set necessary environment variables.
  
- **Docker Compose File:**

```yaml
version: '3'

services:
  nextcloud-aio-mastercontainer:
    image: nextcloud/all-in-one:latest
    container_name: nextcloud-aio-mastercontainer
    init: true
    restart: always
    ports:
      - "8080:8080"
    volumes:
      - nextcloud_aio_mastercontainer:/mnt/docker-aio-config
      - /var/run/docker.sock:/var/run/docker.sock:ro
    environment:
      - NEXTCLOUD_DATADIR=/home/extadmin/nextcloud/data
      - APACHE_PORT=11000
      - APACHE_IP_BINDING=127.0.0.1
      - SKIP_DOMAIN_VALIDATION=true

volumes:
  nextcloud_aio_mastercontainer:
    name: nextcloud_aio_mastercontainer
```

#### Nginx & Certbot Configuration

- **Installation & Setup:**  
  - Updated system packages:

  ```bash
  sudo apt update && sudo apt upgrade
  ```
  
  - Installed Nginx:

  ```bash
  sudo apt install nginx
  ```
  
  - Installed Certbot with the Nginx plugin:

  ```bash
  sudo apt install certbot python3-certbot-nginx
  ```
  
  - Started and enabled Nginx:

  ```bash
  sudo systemctl start nginx
  sudo systemctl enable nginx
  ```

- **Nginx Configuration:**  
  - A configuration file (`nextcloud.conf`) was created in `/etc/nginx/sites-available/` to:
    - Redirect HTTP traffic to HTTPS.
    - Serve ACME challenge requests for SSL.
    - Proxy HTTPS traffic to the Nextcloud container on port 11000.
  - **Example Configuration:**

  ```nginx
  map $http_upgrade $connection_upgrade {
      default upgrade;
      '' close;
  }

  # HTTP server block – redirect to HTTPS
  server {
      listen 80;
      listen [::]:80;
      server_name nextcloud-az.bul-si.bg;
      
      location /.well-known/acme-challenge/ {
          root /var/www/certbot;
      }
      
      location / {
          return 301 https://$host$request_uri;
      }
  }

  # HTTPS server block – proxy to container
  server {
      server_name nextcloud-az.bul-si.bg;
      
      location /.well-known/acme-challenge/ {
          root /var/www/certbot;
      }
      
      location / {
          proxy_pass http://127.0.0.1:11000$request_uri;
          proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
          proxy_set_header X-Forwarded-Port $server_port;
          proxy_set_header X-Forwarded-Scheme $scheme;
          proxy_set_header X-Forwarded-Proto $scheme;
          proxy_set_header X-Real-IP $remote_addr;
          proxy_set_header Accept-Encoding "";
          proxy_set_header Host $host;
          client_body_buffer_size 512k;
          proxy_read_timeout 86400s;
          client_max_body_size 0;
          # WebSocket support
          proxy_http_version 1.1;
          proxy_set_header Upgrade $http_upgrade;
          proxy_set_header Connection $connection_upgrade;
      }
  }
  ```

  - Enabled the configuration:

  ```bash
  sudo ln -s /etc/nginx/sites-available/nextcloud.conf /etc/nginx/sites-enabled/
  sudo nginx -t
  sudo systemctl reload nginx
  ```

- **SSL Certificate:**  
  - Ran Certbot to obtain and install an SSL certificate:

  ```bash
  sudo certbot --nginx -d nextcloud-az.bul-si.bg
  ```

#### Final Steps
- **Container Startup:**  
  Navigated to the Nextcloud directory and started the container:

  ```bash
  cd ~/nextcloud
  docker-compose up -d
  ```

- **Verification:**  
  Accessed the Nextcloud master container via [https://internal.ip.of.this.server:8080](https://internal.ip.of.this.server:8080) to proceed with the UI-based configuration.

---

## 5. Set Up Nextcloud, Backups & Test Recovery

### Overview
After the Nextcloud master container was running, the Nextcloud UI was used to complete the configuration, set up backups, and test recovery. **(Screenshot placeholders are provided at key steps.)**

### What Was Done

#### Nextcloud UI Setup

1. **Step 1:**  
   - Access the Nextcloud interface by navigating to:  
     `https://server-ip:8080`
   - **Action:** Click **Accept the Risk and Continue**.  
   - **[Screenshot Placeholder: Initial UI with risk acceptance prompt]**

2. **Step 2:**  
   - Copy the passphrase required for configuration.  
   - **Location of Passphrase:**  
     `/home/docker-data/volumes/nextcloud_aio_mastercontainer/_data/data/configuration.json`
   - **[Screenshot Placeholder: Display of configuration.json showing passphrase]**

3. **Step 3:**  
   - Paste the copied passphrase into the appropriate field in the UI.  
   - **[Screenshot Placeholder: UI with passphrase pasted]**

4. **Step 4:**  
   - Configure the domain name for which the SSL certificate was obtained and click **Submit domain**.  
   - **Note:** With the `SKIP_DOMAIN_VALIDATION` flag enabled, any domain is accepted; however, if it does not match the domain configured on Nginx with the SSL certificate, it will not function properly.  
   - **[Screenshot Placeholder: Domain configuration screen]**

5. **Step 5:**  
   - Select the services to run with Nextcloud.  
   - **Action:**  
     - Click on each service to choose it, then click **Save**.
     - Scroll down, choose the timezone, and click **Submit**.
     - Click **Download and start containers**.
   - **Note:** Additional containers can be added or removed after the initial setup.
   - **[Screenshot Placeholder: Service selection and timezone configuration]**

6. **Step 6:**  
   - Wait for the containers to start as images are pulled and containers are initiated.  
   - **[Screenshot Placeholder: Progress indicator or log output]**

7. **Step 7:**  
   - Once all containers are running, the admin credentials are provided and backup configuration is available.
   - **Backup & Restore Fields:**
     - **Local Backup Location:**  
       Typically points to an external drive mounted at `/mnt/ncbackups`. Ensure the drive is mounted and accessible.
     - **Remote Borg Repo:**  
       Option for a drive accessible via SSH (not locally mounted).
   - **[Screenshot Placeholder: Backup and restore configuration screen]**

8. **Step 8:**  
   - After entering the backup locations, click **Submit backup location**.
   - **[Screenshot Placeholder: Confirmation of backup location submission]**

9. **Step 9:**  
   - Create the first backup.
   - **Note:**  
     - Borg backups are incremental and encrypted.
     - The encryption password can be found in the same configuration file as the passphrase:  
       `/home/docker-data/volumes/nextcloud_aio_mastercontainer/_data/data/configuration.json`
   - **[Screenshot Placeholder: Backup initiation screen]**

10. **Step 10:**  
    - Once the backup completes, click **Start containers**.
    - **[Screenshot Placeholder: Confirmation that containers have started]**

11. **Step 11:**  
    - Open your Nextcloud instance and log in with the admin credentials.
    - **[Screenshot Placeholder: Nextcloud login screen]**

#### Backup Management (Post-Setup)

- **Backup Options:**
  1. In the Nextcloud AIO interface, click **Click here to reveal all backup options**.  
     **[Screenshot Placeholder: Reveal backup options]**
  2. To trigger a backup, click **Create backup**.  
     **[Screenshot Placeholder: Create backup button]**
  3. To restore a backup, select the desired backup and click **Restore selected backup**.  
     **[Screenshot Placeholder: Backup selection and restore option]**
  4. To configure automatic backups and updates, select a low-usage time (e.g., during the night) and click **Submit backup time**.  
     **[Screenshot Placeholder: Automatic backup configuration]**

### Observations & Outcomes
- **User-Friendly Process:**  
  The Nextcloud UI provides a straightforward method to configure backups and restore data.
- **Performance Considerations:**  
  The duration of backup and restore operations depends on the volume of data stored.
- **Flexibility:**  
  Backup configurations can be modified post-deployment, ensuring adaptability to future storage or performance needs.

---

## 6. Adjust Nextcloud AIO User Limit

### Overview
Nextcloud AIO imposes a configuration limit of 100 users per instance—a limit that resets to 100 with every Nextcloud update. To prevent issues with user creation, a script was developed to automatically update this limit to 10,000.

### What Was Done

#### Script Implementation

- **Configuration File Location:**  
  `/home/docker-data/volumes/nextcloud_aio_nextcloud/_data/config/aio.config.php`

- **Script Functionality:**  
  - Checks if the configuration line for `'one-click-instance.user-limit'` is set to 100.
  - If so, uses `sed` to change the value to 10,000.
  - Logs the action (or lack thereof) to `/var/log/adjust_user_limit.log`.

#### Script Code

```bash
#!/bin/bash

# Path to the aio.config.php file
CONFIG_FILE="/home/docker-data/volumes/nextcloud_aio_nextcloud/_data/config/aio.config.php"

# Check if the config file exists
if [[ -f "$CONFIG_FILE" ]]; then
    # Check if the user limit is set to 100
    if grep -q "'one-click-instance.user-limit' => 100," "$CONFIG_FILE"; then
        # Update the user limit to 10000
        sed -i "s/'one-click-instance.user-limit' => 100,/'one-click-instance.user-limit' => 10000,/" "$CONFIG_FILE"
        echo "$(date): User limit updated to 10000." >> /var/log/adjust_user_limit.log
    else
        echo "$(date): User limit is not 100; no changes made." >> /var/log/adjust_user_limit.log
    fi
else
    echo "$(date): Config file not found." >> /var/log/adjust_user_limit.log
fi
```

#### Cron Job Setup

- The script is scheduled to run once a day via a cron job.

**Cron Entry:**

```cron
# Adjust Nextcloud AIO user limit to 10000 
0 2 * * * /home/extadmin/nextcloud/scripts/adjust_user_limit.sh
```

### Observations & Outcomes
- **Automation Benefits:**  
  The script ensures that the user limit remains high even after updates, preventing disruptions in user management.
- **Logging:**  
  Logging helps track the script’s activity and diagnose any issues if the configuration file is missing or the limit is already adjusted.
- **Maintenance:**  
  This approach minimizes manual intervention after each update, enhancing operational efficiency.
